<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdateProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'name' => 'required',
            'photo' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $profile = Profile::where('email',auth()->email)->first();
        $profile->update([
            'name' => $request->name,
            'photo' => $request->photo
        ]);
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile telah berhasil diupdate',
            'data' => $profile
        ],200);
    }
}
