<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'email' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $request->email)->first();

        if (!$user){
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email Tidak ditemukan'
            ],400);
        }
        $user->update([
            'password' => Hash::make($request->password)
        ]);
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Password berhasil diubah',
            'data' => $user
        ],200);
    }
}
