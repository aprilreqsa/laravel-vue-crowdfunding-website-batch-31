<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Mail\UserRegisteredMail;
use App\OtpCode;
use App\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::create($allRequest);
        $profile = Profile::create([
            'name' => $user->name,
            'email' => $user->email
        ]);
        

        do {
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp',$random)->first();
        }while($check);

        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);

        event(new UserRegisteredEvent($user));
        // Mail::to($user)->send(new UserRegisteredMail($otp_code));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'silahkan cek mail',
            'data' => [
                'user' => $user,
            ]
        ]);
    }
}
