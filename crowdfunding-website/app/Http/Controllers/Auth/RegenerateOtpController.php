<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegenerateOtpEvent;
use App\Http\Controllers\Controller;
use App\Mail\UserRegenerateOtp;
use App\User;
use Carbon\Carbon;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'email' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $request->email)->first();

        if ($user->otp_code){
            $user->otp_code()->delete();
        }
        
        do {
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp',$random)->first();
        }while($check);
        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);
        event(new UserRegenerateOtpEvent($user));
        
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Silahkan cek mail',
            'data' => [
                'user' => $user,
            ]
        ]);
        
    }
}
