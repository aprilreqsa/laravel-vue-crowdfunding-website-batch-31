<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\Profile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'otp' => 'required',
        ]);
        $otp_code = OtpCode::where('otp',$request->otp)->first();

        if (!$otp_code)
        {
            return response()->json([
                'response_code' => '01',
                'message' => 'Otp Code Tidak ditemukan'
            ],400);
        }
        $now = Carbon::now();
        if ($now > $otp_code->valid_until){
            return response()->json([
                'response_code' => '01',
                'message' => 'Otp Code Tidak berlaku lagi'
            ],400);
        }
        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);
        $profile = Profile::where('email',$user->email)->first();
        $profile->update([
            'email_verified_at' => $user->email_verified_at
        ]);
        $otp_code->delete();
        return response()->json([
            'response_code'=> '01',
            'response_message'=> 'User berhasil Verifikasi',
            'data' => $user
        ]);
    }
}
