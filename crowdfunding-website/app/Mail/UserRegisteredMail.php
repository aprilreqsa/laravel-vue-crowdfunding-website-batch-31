<?php

namespace App\Mail;

use App\OtpCode;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $otp_code;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('aprilreqsa2@gmail.com')
                    ->view('send_email_user_registered')
                    ->with([
                        'name' => $this->user->name,
                        'otp' => $this->user->otp->otp
                    ]);
    }
}
