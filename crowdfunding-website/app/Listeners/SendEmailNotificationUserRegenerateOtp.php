<?php

namespace App\Listeners;

use App\Events\UserRegenerateOtpEvent;
use App\Mail\UserRegenerateOtp;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationUserRegenerateOtp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegenerateOtpEvent  $event
     * @return void
     */
    public function handle(UserRegenerateOtpEvent $event)
    {
        Mail::to($event->user)->send(new UserRegenerateOtp($event->user));
    }
}
