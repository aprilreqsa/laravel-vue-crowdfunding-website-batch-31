<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'prefix'=>'auth',
    'namespace'=> 'Auth'
], function(){
    Route::post('register','RegisterController')->name('auth.register');
    Route::post('regenerate-otp','RegenerateOtpController')->name('auth.regenerate-otp');
    Route::post('verification','VerificationController')->name('auth.verification');
    Route::post('update-password','UpdatePasswordController')->name('auth.update-password');
    Route::post('login','LoginController')->name('auth.login');
});
// Route::group([
//     'prefix'=>'profile',
//     'namespace'=> 'Profile',
// ], function(){
//     Route::get('get-profile','GetProfileController')->name('profile.get');
//     Route::post('update-profile','UpdateProfileController')->name('profile.update');   
// })->middleware('auth:api');

Route::apiResource('/profile/get-profile', 'GetProfileController')->middleware('auth:api');
Route::apiResource('/profile/update-profile', 'UpdateProfileController')->middleware('auth:api');
