<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        App\User::create([
            'email' => 'admin@gmail.com',
            'name' => 'admin',
            'role_id' => 2,
            'password' => bcrypt('admin'),
            'email_verified_at' => Carbon::now()
        ]);
        App\User::create([
            'email' => 'user@gmail.com',
            'name' => 'user',
            'role_id' => 1,
            'password' => bcrypt('user'),
        ]);
        App\User::create([
            'email' => 'admin1@gmail.com',
            'name' => 'admin',
            'role_id' => 2,
            'password' => bcrypt('admin'),
        ]);
        
    }
}
