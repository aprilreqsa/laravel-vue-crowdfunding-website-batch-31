<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight {
    use Hewan;
    public $attackPower;
    public $deffencePower;
    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
    }
    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        $this->darah = $this->darah - ($hewan->attackPower / $this->deffencePower);

    }
    abstract public function getInfoHewan();
}

class Elang extends Fight {
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->deffencePower = 5;
    }
    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Deffence Power : {$this->deffencePower}";
        echo "<br>";
    }
}

class Harimau extends Fight {
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->deffencePower = 8;
    }
    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Deffence Power : {$this->deffencePower}";
        echo "<br>";
    }
}

$harimau1 = new Harimau("harimau1");
$harimau1->atraksi();
echo "<br>";
$harimau1->getInfoHewan();
echo "<br>";
$elang1 = new Elang("elang1");
$elang1->atraksi();
echo "<br>";
$elang1->getInfoHewan();
echo "<br>";
$harimau1->serang($elang1);
echo "<br>";
$harimau1->atraksi();
echo "<br>";
$harimau1->getInfoHewan();
echo "<br>";
$elang1->atraksi();
echo "<br>";
$elang1->getInfoHewan();
echo "<br>";

